package com.ta.springmvc.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.ta.springmvc.Model.Article;
import com.ta.springmvc.Service.ArticleService;


@Controller
public class ArticleController {

	   @Autowired
	    ArticleService service;

	    @RequestMapping("/")
	    public String getAllArticles(Model model){
	        List<Article> listArticle = service.getAllArticle();
	        model.addAttribute("listArticle", listArticle);
	        return "index.html";
	    }
	    @RequestMapping("/creationArticle")
	    public String creationArticleView(Model model){
	        Article art = new Article();
	        model.addAttribute("article", art);
	        return "creationArticle.html";
	    }

	    @RequestMapping(value = "/save", method = RequestMethod.POST)
	    public String saveArticle(@ModelAttribute("article") Article article) {
	        service.createOneArticle(article);
	        return "redirect:/";
	    }

	    @RequestMapping(value = "/edit", method = RequestMethod.POST)
	    public String updateArticle(@ModelAttribute("article") Article article) {
	        service.updateOneArticle(article, article.getName());
	        return "redirect:/";
	    }

	    @RequestMapping("/edit/{name}")
	    public ModelAndView showEditArticlePage(@PathVariable(name = "name") String name) {
	        ModelAndView mav = new ModelAndView("updateArticle.html");
	        Article article = service.getOneArticle(name);
	        mav.addObject("article", article);
	        return mav;
	    }

	    @RequestMapping("/delete/{name}")
	    public String showDeleteArticlePage(@PathVariable(name = "name") String name) {
	        service.deleteOneArticle(name);
	        return "redirect:/";
	    }


	    @RequestMapping("/detail/{name}")
	    public ModelAndView showDetailArticlePage(@PathVariable(name = "name") String name) {
	        ModelAndView mav = new ModelAndView("article.html");
	        Article article = service.getOneArticle(name);
	        mav.addObject("article", article);

	        return mav;

	    }

	}
