package com.ta.springmvc.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.ta.springmvc.Model.OneArticleInPanier;
import com.ta.springmvc.Service.PanierService;

import java.util.List;

@Controller
public class PanierController {

    @Autowired
    PanierService panierService;

    @RequestMapping("/panier")
    public String getPanier(Model model) {
        List<OneArticleInPanier> listArticles = panierService.getPanier();
        int sommeTot = panierService.getSommeTot();
        model.addAttribute("listArticles", listArticles);
        model.addAttribute("sommeTot", sommeTot);
        return "panier.html";
    }

    @RequestMapping("/add/{name}/{price}")
    public String addArticleToPanier(@PathVariable(name = "name") String name,@PathVariable(name = "price") float price) {
        panierService.addOneArticle(name, price);
        return "redirect:/panier";
    }

    @RequestMapping(value = "/deletefrompanier/{name}", method = RequestMethod.GET)
    public String deleteArticleFromPanier(@PathVariable(name = "name") String name) {
        panierService.deleteOneArticle(name);
        return "redirect:/";
    }

    @RequestMapping("/editpanier")
    public String updateArticleFromPanier(Model model) {
        List<OneArticleInPanier> listArticles = panierService.getPanier();
        model.addAttribute("listArticles", listArticles);
        return "updatePanier.html";
    }


}
