package com.ta.springmvc.Model;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class OneArticleInPanier {

    @Id
    String name;
    float price;

    public OneArticleInPanier(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
