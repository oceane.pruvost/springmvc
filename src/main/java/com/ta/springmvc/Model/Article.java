package com.ta.springmvc.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Article {


    public String name;
    public String description;
    public float price;

    public Article(){}

    @Id
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
