package com.ta.springmvc.DAO;

import org.springframework.stereotype.Repository;
import com.ta.springmvc.Model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ArticleDAO extends JpaRepository<Article, String> {

    default void updateArticle(Article article, String name){
        if(this.findById(name).isPresent()) {
            this.deleteById(name);
            this.save(article);
        }
    }
}
