package com.ta.springmvc.DAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ta.springmvc.Model.OneArticleInPanier;

@Repository
public interface OneArticleInPanierDAO extends JpaRepository<OneArticleInPanier, String> {


    default void updatePanier(OneArticleInPanier art, String name){
        if(this.findById(name).isPresent()){
            this.deleteById(name);
            this.save(art);
        }
    }

}
