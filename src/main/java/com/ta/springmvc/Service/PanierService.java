package com.ta.springmvc.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ta.springmvc.DAO.ArticleDAO;
import com.ta.springmvc.DAO.OneArticleInPanierDAO;
import com.ta.springmvc.Model.Article;
import com.ta.springmvc.Model.OneArticleInPanier;
import java.util.List;
import java.util.Optional;

@Service
public class PanierService {

    @Autowired
    OneArticleInPanierDAO dao;
    ArticleDAO daoArt;

    public List<OneArticleInPanier> getPanier(){
        return dao.findAll();
    }

    public void addOneArticle(String name, float price){
        OneArticleInPanier newArticle = new OneArticleInPanier();
        newArticle.setName(name);
        newArticle.setPrice(price);
        dao.save(newArticle);
    }

    public void updatePanier(Article article, int quantity){
       Optional<OneArticleInPanier> art = dao.findById(article.getName()); //car il peut ne rien trouver
       if(art.isPresent()){
           OneArticleInPanier updateArticle = new OneArticleInPanier();
           updateArticle.setName(art.get().getName());
           updateArticle.setPrice(art.get().getPrice());
           dao.updatePanier(updateArticle, updateArticle.getName());
       }
    }

    public int getSommeTot(){
        int sommeTot = 0;
        List<OneArticleInPanier> listArticles = dao.findAll();
        for (OneArticleInPanier a :listArticles
             ) {
            sommeTot += a.getPrice();
        }
        return sommeTot;
    }

    public void deleteOneArticle(String name){
        OneArticleInPanier newArticle = new OneArticleInPanier();
        newArticle.setName(name);
            dao.deleteById(name);
    }
}