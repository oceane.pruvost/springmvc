package com.ta.springmvc.Service;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ta.springmvc.DAO.ArticleDAO;
import com.ta.springmvc.Model.Article;


@Service
@Transactional
public class ArticleService {

    @Autowired
    ArticleDAO articleDAO;

    public Article getOneArticle(String name) {
        return articleDAO.findById(name).get();
    }

    public void updateOneArticle(Article article, String name){
        articleDAO.updateArticle(article, name);
    }

    public void deleteOneArticle(String name){
        articleDAO.deleteById(name);
    }

    public void createOneArticle(Article article){
        articleDAO.save(article);
    }

    public List<Article> getAllArticle() {
        return articleDAO.findAll();
    }
}
